## Testing

* copy `tests/config.php.copy` to `tests/config.php`

* add your p12 file to `tests/testing.p12`

* edit `tests/config.php` to match your environment
    * impersonate - an email address that oauth_service has access to the domain/address
    * client_id - the client id from your oauth_service account
    * email_address - the email address from your oauth_service account
    * path_to_p12 - path to the p12 file used for testing (probably `tests/testing.p12`)

Integration tests will attempt to access the primary calendar resource for the user
you are impersonating.  Calendar scopes must be added to the developer's console and,
more than likely, to the domain you are administering.  I'm not sure if adding the email
address of the oauth_service account to the impersonated calendar would work...

Unit tests simple play through the logic.  They do require a file to exist at `tests/testing.p12`