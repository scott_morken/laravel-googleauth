<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/19/14
 * Time: 10:43 AM
 */

namespace Smorken\GoogleAuth;

use Smorken\GoogleAuth\Model\VO\AccessToken;
use Smorken\GoogleAuth\Model\VO\AuthItem;

class Auth implements \Smorken\GoogleAuth\Contracts\Auth
{

    /**
     * @var Manager
     */
    protected \Smorken\GoogleAuth\Contracts\Manager $manager;

    public function __construct(\Smorken\GoogleAuth\Contracts\Manager $manager)
    {
        $this->manager = $manager;
    }

    public function getAuthUrl(): string
    {
        return $this->manager->getClient(self::OAUTH_WEB)->createAuthUrl();
    }

    public function getClient(?string $type = null, ?string $as_user = null): \Google_Client|false
    {
        $ai = $this->manager->getAuthItem($type, $as_user);
        $t = $ai->getToken()->tokenRequest();
        if ($t === true || ($t instanceof AccessToken && $t->active())) {
            return $ai->getClient();
        }
        return false;
    }

    public function impersonate(string $as_user, ?string $type = null): void
    {
        if ($type === null) {
            $type = self::OAUTH_SERVICE;
        }
        $this->manager->impersonate($as_user, $type);
    }

    public function isExpired(?string $type = null): bool
    {
        if ($type) {
            $ai = $this->manager->getAuthItem($type);
            return $this->checkExpiredItem($ai);
        } else {
            $ais = $this->manager->getAuthItems();
            foreach ($ais as $ai) {
                if (!$this->checkExpiredItem($ai)) {
                    return false;
                }
            }
            return true;
        }
    }

    public function logout(?string $type = null): void
    {
        if ($type) {
            $ai = $this->manager->getAuthItem($type);
            $this->revoke($ai);
        } else {
            $ais = $this->manager->getAuthItems();
            foreach ($ais as $ai) {
                $this->revoke($ai);
            }
        }
    }

    protected function checkExpiredItem(AuthItem $ai): bool
    {
        return $ai->getToken()->isExpired();
    }

    protected function revoke(AuthItem $ai): void
    {
        $ai->getToken()->revoke();
    }

}
