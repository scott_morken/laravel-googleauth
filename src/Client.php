<?php

namespace Smorken\GoogleAuth;

class Client implements \Smorken\GoogleAuth\Contracts\Client
{

    /**
     * @var \Google_Client
     */
    protected \Google_Client $client;

    public function __construct(\Google_Client $client)
    {
        $this->client = $client;
    }

    public function __call(string $name, array $arguments): mixed
    {
        return call_user_func_array([$this, $name], $arguments);
    }

    /**
     * @return \Google_Client
     */
    public function getClient(): \Google_Client
    {
        return clone $this->client;
    }
}
