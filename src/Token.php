<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/20/14
 * Time: 7:24 AM
 */

namespace Smorken\GoogleAuth;

use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Request;
use Smorken\GoogleAuth\Model\VO\AccessToken;
use Smorken\GoogleAuth\Model\VO\AuthItem;

class Token
{

    /**
     * @var AuthItem|null
     */
    protected ?AuthItem $authitem = null;

    /**
     * @var \Illuminate\Session\SessionManager
     */
    protected SessionManager $session;

    protected ?array $token = null;

    public function __construct(\Illuminate\Session\SessionManager $session)
    {
        $this->session = $session;
    }

    public function getToken(): ?array
    {
        return $this->token;
    }

    public function setToken(array $token): void
    {
        $this->addTokenToSession($token);
        $this->token = $token;
    }

    public function isExpired(): bool
    {
        return $this->getClient()->isAccessTokenExpired();
    }

    public function revoke(): void
    {
        $this->session->forget($this->getSessionKey());
        $this->getClient()->revokeToken();
        $this->token = null;
    }

    /**
     * @param  AuthItem  $ai
     */
    public function setAuthItem(AuthItem $ai): void
    {
        $this->authitem = $ai;
    }

    public function tokenRequest(): AccessToken|bool
    {
        if ($this->requiresToken()) {
            $this->setTokenFromSession();
            if (!$this->getClient()->getAccessToken() || $this->getClient()->isAccessTokenExpired()) {
                $token = $this->refreshToken();
            } else {
                $token = $this->getClient()->getAccessToken();
            }
            $this->setToken($token);
            return new AccessToken($token);
        }
        return true;
    }

    protected function addTokenToSession(array $token): void
    {
        $this->session->put($this->getSessionKey(), $token);
    }

    /**
     * @return \Google_Client
     */
    protected function getClient(): \Google_Client
    {
        return $this->authitem->getClient();
    }

    /**
     * @return array
     */
    protected function getConfig(): array
    {
        return $this->authitem->getConfig();
    }

    /**
     * @param $key
     * @param  null  $default
     * @return mixed|null
     */
    protected function getConfigItem(string $key, mixed $default = null): mixed
    {
        return $this->authitem->getConfigItem($key, $default);
    }

    protected function getSessionKey(): string
    {
        return sprintf("%s%s%s", $this->getConfigItem('session_token', 'gauth'), $this->getType(), md5(get_class()));
    }

    /**
     * Helper functions to shorten typing
     */

    /**
     * @return string
     */
    protected function getType(): string
    {
        return $this->authitem->getType();
    }

    protected function refreshToken(): array
    {
        if ($this->getType() === Contracts\Auth::OAUTH_SERVICE) {
            $this->getClient()->fetchAccessTokenWithAssertion();
        } else {
            $code = Request::input('code');
            if ($code) {
                $this->getClient()->fetchAccessTokenWithAuthCode($code);
            }
        }
        return $this->getClient()->getAccessToken();
    }

    protected function requiresToken(): bool
    {
        $requires = [Contracts\Auth::OAUTH_SERVICE, Contracts\Auth::OAUTH_WEB];
        return in_array($this->getType(), $requires);
    }

    protected function setTokenFromSession(): void
    {
        if ($this->session->has($this->getSessionKey())) {
            $this->getClient()->setAccessToken($this->session->get($this->getSessionKey()));
        }
    }
}
