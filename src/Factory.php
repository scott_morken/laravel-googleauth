<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/20/14
 * Time: 7:21 AM
 */

namespace Smorken\GoogleAuth;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Session\SessionManager;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\GoogleAuth\Model\VO\AuthItem;

class Factory
{

    /**
     * @var array
     */
    protected array $config = [];

    /**
     * @var Filesystem
     */
    protected \Illuminate\Filesystem\Filesystem $filesystem;

    /**
     * @var SessionManager
     */
    protected SessionManager $session;

    /**
     * @param  Filesystem  $filesystem
     * @param  SessionManager  $session
     */
    public function __construct(Filesystem $filesystem, SessionManager $session)
    {
        $this->filesystem = $filesystem;
        $this->session = $session;
    }

    /**
     * @param  \Google_Client  $client
     * @param  string  $type
     * @param  array  $config
     * @param  null|string  $as_user
     * @return mixed
     */
    public function make(
        \Google_Client $client,
        #[ExpectedValues(valuesFromClass: \Smorken\GoogleAuth\Contracts\Auth::class)] string $type,
        array $config,
        ?string $as_user = null
    ): AuthItem {
        $this->config = $config;
        $client->setApplicationName($this->getConfigItem('application_name'));
        $client->setAccessType($this->getConfigItem('access_mode', 'offline'));
        switch ($type) {
            case Contracts\Auth::OAUTH_SERVICE:
                $this->handleOauthService($client, $as_user);
                break;
            case Contracts\Auth::OAUTH_WEB:
                $client->setClientId($this->getConfigItem('client_id'));
                $client->setClientSecret($this->getConfigItem('client_secret'));
                $client->setScopes($this->getConfigItem('scopes', []));
                $client->setRedirectUri($this->getConfigItem('redirect_uri'));
                break;
            case Contracts\Auth::PUBLIC_SERVER:
            case Contracts\Auth::PUBLIC_BROWSER:
                $client->setDeveloperKey($this->getConfigItem('developer_key'));
                break;
            default:
                throw new \InvalidArgumentException("Invalid access type [$type] given.");
                break;
        }
        $token = new Token($this->session);
        return new AuthItem($client, $type, $config, $token);
    }

    public function makeAssertionCredentials(?string $as_user = null): array
    {
        $this->checkRequired(['path_to_p12', 'email_address', 'scopes']);
        return [
            'type' => 'service_account',
            'client_id' => $this->getConfigItem('client_id'),
            'client_email' => $this->getConfigItem('email_address'),
            'private_key' => $this->getPrivateKeyFromP12(),
        ];
    }

    protected function checkRequired(array $reqs): void
    {
        foreach ($reqs as $req) {
            if (!isset($this->config[$req])) {
                throw new \InvalidArgumentException("$req is a required parameter.");
            }
        }
    }

    protected function getConfigItem(?string $key, mixed $default = null): mixed
    {
        if (!$key || !$this->config) {
            return $default;
        }
        return $this->config[$key] ?? $default;
    }

    protected function getPrivateKeyFromP12(): ?string
    {
        $file = $this->getConfigItem('path_to_p12');
        if ($this->filesystem->exists($file)) {
            $contents = $this->filesystem->get($file);
            if (openssl_pkcs12_read($contents, $certs, $this->getConfigItem('p12_password'))) {
                return $certs['pkey'] ?? null;
            } else {
                throw new Exception(openssl_error_string());
            }
        }
        return null;
    }

    protected function handleOauthService(\Google_Client $client, ?string $as_user = null): void
    {
        $creds_file = $this->getConfigItem('credentials_file');
        if ($creds_file) {
            $creds = json_decode($this->filesystem->get($creds_file), true);
        } else {
            $creds = $this->makeAssertionCredentials($as_user);
        }
        $scopes = $this->getConfigItem('scopes', []);
        $client->setScopes($scopes);
        $client->setAuthConfig($creds);
        if ($as_user) {
            $client->setSubject($as_user);
        }
    }
}
