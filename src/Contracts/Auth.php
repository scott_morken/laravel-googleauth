<?php

namespace Smorken\GoogleAuth\Contracts;

interface Auth
{

    const OAUTH_SERVICE = 'oauth_service';
    const OAUTH_WEB = 'oauth_web';
    const PUBLIC_BROWSER = 'public_browser';
    const PUBLIC_SERVER = 'public_server';

    /**
     * @return string
     */
    public function getAuthUrl(): string;

    /**
     * @param  string|null  $type
     * @param  string|null  $as_user
     * @return \Google_Client|false
     */
    public function getClient(?string $type = null, ?string $as_user = null): \Google_Client|false;

    /**
     * @param  string  $as_user
     * @param  string|null  $type
     */
    public function impersonate(string $as_user, ?string $type = null): void;

    /**
     * @param  string|null  $type
     * @return bool
     */
    public function isExpired(?string $type = null): bool;
}
