<?php

namespace Smorken\GoogleAuth\Contracts;

use Smorken\GoogleAuth\Model\VO\AuthItem;

interface Manager
{

    /**
     * @param  string|null  $type
     * @param  string|null  $as_user
     * @return AuthItem
     */
    public function getAuthItem(?string $type = null, ?string $as_user = null): AuthItem;

    /**
     * @return array
     */
    public function getAuthItems(): array;

    /**
     * @param  string|null  $type
     * @param  string|null  $as_user
     * @return \Google_Client
     */
    public function getClient(?string $type = null, ?string $as_user = null): \Google_Client;

    /**
     * @param  string|null  $type
     * @return array
     */
    public function getConfig(?string $type = null): array;

    /**
     * @param  string  $type
     * @param  string  $key
     * @param  null  $default
     * @return mixed
     */
    public function getConfigItem(string $type, string $key, mixed $default = null): mixed;

    /**
     * @param  string|null  $type
     * @return array
     */
    public function getScopes(?string $type = null): array;

    /**
     * @param  string  $as_user
     * @param  string|null  $type
     * @return bool
     */
    public function impersonate(string $as_user, ?string $type = null): bool;

    /**
     * @param  array  $config
     * @param  string|null  $type
     */
    public function setConfig(array $config, ?string $type = null): void;

    /**
     * @param  array  $scopes
     * @param  string|null  $type
     */
    public function setScopes(array $scopes, ?string $type = null): void;
}
