<?php


namespace Smorken\GoogleAuth\Contracts;


interface Client
{

    /**
     * @return \Google_Client
     */
    public function getClient(): \Google_Client;
}
