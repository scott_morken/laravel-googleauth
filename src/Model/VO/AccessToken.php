<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/19/14
 * Time: 12:11 PM
 */

namespace Smorken\GoogleAuth\Model\VO;

class AccessToken
{

    protected array $attributes = [
        'access_token' => null,
        'expires_in' => null,
        'created' => null,
    ];

    public function __construct(array $attributes = [])
    {
        if ($attributes) {
            $this->setAttributes($attributes);
        }
    }

    public function __get(string $key): ?string
    {
        return $this->getAttribute($key);
    }

    public function __set(string $key, ?string $value): void
    {
        $this->setAttribute($key, $value);
    }

    public function active(): bool
    {
        foreach ($this->attributes as $k => $v) {
            if (!$v) {
                return false;
            }
        }
        return true;
    }

    public function getAttribute(string $key): ?string
    {
        return $this->attributes[$key] ?? null;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function setAttributes(array|string $attributes): void
    {
        if (!is_array($attributes)) {
            $attributes = json_decode($attributes, true);
        }
        if (is_array($attributes)) {
            foreach ($this->attributes as $k => $v) {
                if (isset($attributes[$k])) {
                    $this->setAttribute($k, $attributes[$k]);
                }
            }
        }
    }

    public function setAttribute(string $key, ?string $value): void
    {
        $this->attributes[$key] = $value;
    }
}
