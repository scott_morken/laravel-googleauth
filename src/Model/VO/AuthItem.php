<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/20/14
 * Time: 7:48 AM
 */

namespace Smorken\GoogleAuth\Model\VO;

use Smorken\GoogleAuth\Token;

class AuthItem
{

    /**
     * @var \Google_Client
     */
    protected \Google_Client $client;

    /**
     * @var array
     */
    protected array $config = [];

    /**
     * @var Token
     */
    protected Token $token;

    /**
     * @var string
     */
    protected string $type;

    /**
     * @param  \Google_Client  $client
     * @param  string  $type
     * @param  array  $config
     * @param  Token  $token
     */
    public function __construct(
        \Google_Client $client,
        string $type,
        array $config,
        Token $token
    ) {
        $this->setClient($client);
        $this->setType($type);
        $this->setConfig($config);
        $this->setToken($token);
    }

    public function __get(string $key): mixed
    {
        $method = 'get'.ucfirst($key);
        if (method_exists($this, $key)) {
            return $this->$method();
        }
        return null;
    }

    public function __set(string $key, mixed $value): void
    {
        $method = 'set'.ucfirst($key);
        if (method_exists($this, $method)) {
            $this->$method($value);
        }
    }

    /**
     * @return \Google_Client
     */
    public function getClient(): \Google_Client
    {
        return $this->client;
    }

    public function setClient(\Google_Client $client): void
    {
        $this->client = $client;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    public function getConfigItem(string $key, mixed $default = null): mixed
    {
        if (!$key) {
            return $default;
        }
        return $this->config[$key] ?? $default;
    }

    public function getToken(): Token
    {
        return $this->token;
    }

    public function setToken(Token $token): void
    {
        $this->token = $token;
        $this->getToken()->setAuthItem($this);
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
