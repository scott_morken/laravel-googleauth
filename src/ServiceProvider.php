<?php namespace Smorken\GoogleAuth;

use Illuminate\Contracts\Config\Repository;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootConfig();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            \Smorken\GoogleAuth\Contracts\Manager::class,
            function ($app) {
                $client = $app[\Smorken\GoogleAuth\Contracts\Client::class];
                $factory = new Factory($app['files'], $app['session']);
                $config = $app[Repository::class];
                return new Manager($client, $factory, $config);
            }
        );
        $this->app->bind(
            \Smorken\GoogleAuth\Contracts\Auth::class,
            function ($app) {
                $manager = $app[\Smorken\GoogleAuth\Contracts\Manager::class];
                return new Auth($manager);
            }
        );
        $this->app->bind(
            \Smorken\GoogleAuth\Contracts\Client::class,
            function ($app) {
                return new Client(new \Google_Client());
            }
        );
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'googleauth');
        $this->publishes([$config => config_path('googleauth.php')], 'config');
    }

}
