<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/19/14
 * Time: 11:20 AM
 */

namespace Smorken\GoogleAuth\Facade;

use Illuminate\Support\Facades\Facade;
use Smorken\GoogleAuth\Contracts\Auth;

class GoogleAuth extends Facade
{

    protected static function getFacadeAccessor(): string
    {
        return Auth::class;
    }
}
