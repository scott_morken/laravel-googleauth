<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 11/20/14
 * Time: 6:54 AM
 */

namespace Smorken\GoogleAuth;

use Illuminate\Contracts\Config\Repository;
use Smorken\GoogleAuth\Model\VO\AuthItem;

class Manager implements \Smorken\GoogleAuth\Contracts\Manager
{

    /**
     * @var Contracts\Client
     */
    protected \Smorken\GoogleAuth\Contracts\Client $client;

    /**
     * Array of clients
     *
     * @var array
     */
    protected array $clients = [];

    /**
     * Config overrides
     *
     * @var array
     */
    protected array $config = [];

    /**
     * @var Repository
     */
    protected Repository $configProvider;

    /**
     * @var Factory
     */
    protected Factory $factory;

    /**
     * Scopes allowed (override config)
     *
     * @var array
     */
    protected array $scopes = [];

    /**
     * @param  Contracts\Client  $client
     * @param  Factory  $factory
     * @param  Repository  $config
     */
    public function __construct(\Smorken\GoogleAuth\Contracts\Client $client, Factory $factory, Repository $config)
    {
        $this->configProvider = $config;
        $this->client = $client;
        $this->factory = $factory;
    }

    public function canImpersonate(?string $type = null): bool
    {
        $type = $this->getType($type);
        return $type === \Smorken\GoogleAuth\Contracts\Auth::OAUTH_SERVICE;
    }

    /**
     * @param  string|null  $type
     * @param  string|null  $as_user
     * @return AuthItem
     */
    public function getAuthItem(?string $type = null, ?string $as_user = null): AuthItem
    {
        $type = $this->getType($type);
        $key = sprintf('%s.%s', $type, $as_user ?? 'none');
        if (!isset($this->clients[$key])) {
            $this->clients[$key] = $this->makeClient($type, $as_user);
        }
        return $this->clients[$key];
    }

    /**
     * @return AuthItem[]
     */
    public function getAuthItems(): array
    {
        return $this->clients;
    }

    /**
     * @param  string|null  $type
     * @param  null|string  $as_user  email address of user in domain
     * @return \Google_Client
     */
    public function getClient(?string $type = null, ?string $as_user = null): \Google_Client
    {
        return $this->getAuthItem($type, $as_user)->getClient();
    }

    /**
     * @param  string|null  $type
     * @return array
     */
    public function getConfig(?string $type = null): array
    {
        $type = $this->getType($type);
        $orig = $this->configProvider->get($this->getConfigBase(), []);
        $override = $this->config[$type] ?? [];
        return array_merge($orig, $override);
    }

    /**
     * Override config items, needs to be called before the client has been
     * created for $type
     *
     * @param  array  $config
     * @param  null|string  $type
     */
    public function setConfig(array $config, ?string $type = null): void
    {
        $type = $this->getType($type);
        $this->config[$type] = $config;
    }

    /**
     * @param  string|null  $type
     * @param  string  $key
     * @param  null  $default
     * @return mixed|null
     */
    public function getConfigItem(?string $type, string $key, mixed $default = null): mixed
    {
        $config = $this->getConfig($type);
        return $config[$key] ?? $default;
    }

    /**
     * @param  string|null  $type
     * @return array
     */
    public function getScopes(?string $type = null): array
    {
        $type = $this->getType($type);
        if (!$this->scopes || !isset($this->scopes[$type])) {
            return $this->getConfigItem($type, 'scopes', []);
        }
        return $this->scopes[$type] ?? [];
    }

    /**
     * Override scopes set in config, needs to be called before the client has been
     * created for $type
     *
     * @param  array  $scopes
     * @param  null|string  $type
     */
    public function setScopes(array $scopes, ?string $type = null): void
    {
        $type = $this->getType($type);
        $this->scopes[$type] = $scopes;
    }

    /**
     * @param  string  $as_user
     * @param  string|null  $type
     * @return bool
     */
    public function impersonate(string $as_user, ?string $type = null): bool
    {
        $type = $this->getType($type);
        $this->getAuthItem($type, $as_user);
        return $this->canImpersonate($type);
    }

    protected function configKey(string $key): string
    {
        return $this->getConfigBase().'.'.$key;
    }

    protected function getConfigBase(): string
    {
        return 'googleauth';
    }

    protected function getType(?string $type = null): string
    {
        return $type ?: $this->configProvider->get($this->configKey('access_type'), Auth::PUBLIC_SERVER);
    }

    /**
     * @param  string|null  $type
     * @param  null|string  $as_user
     * @return AuthItem
     */
    protected function makeClient(?string $type, ?string $as_user = null): AuthItem
    {
        $client = $this->client->getClient();
        $config = $this->getConfig($type);
        $config['scopes'] = $this->getScopes($type);
        return $this->factory->make($client, $type, $config, $as_user);
    }
}
