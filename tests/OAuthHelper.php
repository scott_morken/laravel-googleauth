<?php
namespace Tests\Smorken\GoogleAuth;
/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Illuminate\Support\Facades\Config;
use Smorken\GoogleAuth\Contracts\Client;

class OAuthHelper extends GoogleAuthTestCase
{

    public function testHelper()
    {
//    \Config::shouldReceive('get')->with('scopes')->andReturn(array(
//        "https://www.googleapis.com/auth/plus.me",
//        "https://www.googleapis.com/auth/urlshortener",
//        "https://www.googleapis.com/auth/tasks",
//        "https://www.googleapis.com/auth/adsense",
//        "https://www.googleapis.com/auth/youtube"
//    ));
        $type = Config::get('googleauth.account_type');

        $client = $this->app->make(Client::class);
        $accessToken = null;
        if ($type === 'web') {
            $client->setRedirectUri("urn:ietf:wg:oauth:2.0:oob");

            $authUrl = $client->createAuthUrl();

            `open '$authUrl'`;
            echo "\nPlease enter the auth code:\n";
            $authCode = trim(fgets(STDIN));

            $accessToken = $client->authenticate($authCode);
        } elseif ($type === 'service') {
            $creds = $this->app->make('google.assertion_credentials');
            $client->getAuth()->refreshTokenWithAssertion($creds);
            $accessToken = $client->getAccessToken();
        }

        echo "\n", 'Add the following to config.php as the $token value:', "\n\n";
        echo $accessToken, "\n\n";
        $this->assertTrue(true);
    }
}
