<?php

namespace Tests\Smorken\GoogleAuth;

use Mockery as m;
use Smorken\GoogleAuth\Contracts\Auth as AuthAlias;

class GoogleAuthTestCase extends \Orchestra\Testbench\TestCase
{

    /**
     * @var array
     */
    protected array $config = [];

    protected array $providers = [];

    /**
     * @var string
     */
    protected string $token = "ya29.wwDpOgm2YnKg3vR4rMFYJoKJaxYqbtaVz81EIPGyquODFuHZCddebfRCUEGQzfPC77V1RMubZGOHlA";

    protected function getConfig(?string $key = null): mixed
    {
        if ($key) {
            return $this->config[$key] ?? null;
        }
        return $this->config;
    }

    protected function getConfigArray(): array
    {
        return [
            'access_type' => AuthAlias::PUBLIC_SERVER,
            'session_token' => 'gauth_token',
            'client_id' => 'randomstring.apps.googleusercontent.com',
            'client_secret' => null,
            'developer_key' => 'randomstring',
            'application_name' => null,
            'redirect_uri' => 'http://localhost',
            'scopes' => [
                'https://www.googleapis.com/auth/userinfo.profile',
                'https://www.googleapis.com/auth/userinfo.email',
            ],
            'access_mode' => 'offline',
            'email_address' => 'randomstring@developer.gserviceaccount.com',
            'path_to_p12' => __DIR__.'/../testing.p12',
            'p12_password' => 'notasecret',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // reset base path to point to our package's src directory
        $app['path.base'] = __DIR__;
    }

    protected function getPackageProviders($app)
    {
        return [\Smorken\GoogleAuth\ServiceProvider::class,];
    }

    protected function mockInstance(string $item, bool $setOnApp = true): mixed
    {
        if (!isset($this->providers[$item])) {
            $mock = m::mock($item);
            $this->providers[$item] = $mock;
            if ($setOnApp) {
                $this->app[$item] = $mock;
            }
        }
        return $this->providers[$item];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = require __DIR__.'/config.php';
        $this->app['config']->set('googleauth', $this->config);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
