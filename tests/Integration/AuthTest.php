<?php

namespace Tests\Smorken\GoogleAuth\Integration;

use Smorken\GoogleAuth\Contracts\Auth;
use Tests\Smorken\GoogleAuth\GoogleAuthTestCase;

class AuthTest extends GoogleAuthTestCase
{

    /**
     * @var Auth|null
     */
    protected ?Auth $sut = null;

    public function testAuthUrl(): void
    {
        $this->setAccessType('oauth_service');
        $sut = $this->sut;
        $this->assertStringStartsWith(
            'https://accounts.google.com/o/oauth2/auth?response_type=code',
            $sut->getAuthUrl()
        );
    }

    public function testGetGoogleServiceWithOauthServiceFromJson(): void
    {
        $this->setAccessType('oauth_service');
        $this->app['config']->set(
            'googleauth.scopes',
            [
                'https://www.googleapis.com/auth/calendar',
            ]
        );
        $sut = $this->sut;
        $client = $sut->getClient(Auth::OAUTH_SERVICE, $this->getConfig('impersonate'));
        $service = new \Google_Service_Calendar($client);
        $results = $service->calendars->get('primary');
        $this->assertInstanceOf('\Google_Service_Calendar_Calendar', $results);
    }

    public function testGetGoogleServiceWithOauthServiceFromP12(): void
    {
        $this->setAccessType('oauth_service');
        $this->app['config']->set(
            'googleauth.scopes',
            [
                'https://www.googleapis.com/auth/calendar',
            ]
        );
        $this->app['config']->set('googleauth.credentials_file', null);
        $sut = $this->sut;
        $client = $sut->getClient(Auth::OAUTH_SERVICE, $this->getConfig('impersonate'));
        $service = new \Google_Service_Calendar($client);
        $results = $service->calendars->get('primary');
        $this->assertInstanceOf('\Google_Service_Calendar_Calendar', $results);
    }

    public function testGetGoogleServiceWithPublicServer(): void
    {
        $this->setAccessType('public_server');
        $this->app['config']->set(
            'googleauth.scopes',
            [
                'https://www.googleapis.com/auth/books',
            ]
        );
        $sut = $this->sut;
        $client = $sut->getClient();
        $service = new \Google_Service_Books($client);
        $optParams = ['filter' => 'free-ebooks'];
        $results = $service->volumes->listVolumes('Henry David Thoreau', $optParams);
        $this->assertInstanceOf('\Google_Service_Books_Volumes', $results);
    }

    protected function setAccessType(string $type): void
    {
        $this->app['config']->set('googleauth.access_type', $type);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->sut = $this->app[Auth::class];
    }
}
