<?php

namespace Tests\Smorken\GoogleAuth\Unit;

use Mockery as m;
use Smorken\GoogleAuth\Auth;
use Smorken\GoogleAuth\Contracts\Auth as AuthAlias;
use Smorken\GoogleAuth\Contracts\Manager;
use Tests\Smorken\GoogleAuth\GoogleAuthTestCase;

class AuthTest extends GoogleAuthTestCase
{

    /**
     * @var Auth|null
     */
    protected ?Auth $sut = null;

    public function testGetAuthUrl(): void
    {
        $this->mockInstance(Manager::class)->shouldReceive('getClient->createAuthUrl')->andReturn('foo');
        $this->assertEquals('foo', $this->sut->getAuthUrl());
    }

    public function testGetClientActive(): void
    {
        $ai = m::mock('Smorken\GoogleAuth\Model\VO\AuthItem');
        $this->mockInstance(Manager::class)->shouldReceive('getAuthItem')->andReturn($ai);
        $t = m::mock('Smorken\GoogleAuth\Token');
        $ai->shouldReceive('getToken')->andReturn($t);
        $tr = m::mock('Smorken\GoogleAuth\Model\VO\AccessToken');
        $t->shouldReceive('tokenRequest')->andReturn($tr);
        $tr->shouldReceive('active')->andReturn(true);
        $client = new \Google_Client();
        $ai->shouldReceive('getClient')->andReturn($client);
        $this->assertEquals($client, $this->sut->getClient());
    }

    public function testGetClientCanReturnFalse(): void
    {
        $ai = m::mock('Smorken\GoogleAuth\Model\VO\AuthItem');
        $this->mockInstance(Manager::class)->shouldReceive('getAuthItem')->andReturn($ai);
        $t = m::mock('Smorken\GoogleAuth\Token');
        $ai->shouldReceive('getToken')->andReturn($t);
        $t->shouldReceive('tokenRequest')->andReturn(false);
        $this->assertFalse($this->sut->getClient());
    }

    public function testGetClientDefaults(): void
    {
        $ai = m::mock('Smorken\GoogleAuth\Model\VO\AuthItem');
        $this->mockInstance(Manager::class)->shouldReceive('getAuthItem')->andReturn($ai);
        $t = m::mock('Smorken\GoogleAuth\Token');
        $ai->shouldReceive('getToken')->andReturn($t);
        $t->shouldReceive('tokenRequest')->andReturn(true);
        $client = new \Google_Client();
        $ai->shouldReceive('getClient')->andReturn($client);
        $this->assertEquals($client, $this->sut->getClient());
    }

    public function testLogoutMutiple(): void
    {
        $t = m::mock('Smorken\GoogleAuth\Token');
        $ais = [];
        for ($i = 0; $i < 3; $i++) {
            $ai = m::mock('Smorken\GoogleAuth\Model\VO\AuthItem');
            $ai->shouldReceive('getToken')->andReturn($t);
            $ai->shouldReceive('isExpired')->andReturn(true);
            $ais[] = $ai;
        }
        $this->mockInstance(Manager::class)->shouldReceive('getAuthItems')->andReturn($ais);
        $t->shouldReceive('revoke')->times(3);
        $t->shouldReceive('isExpired')->times(3)->andReturn(true);
        $this->sut->logout();
        $this->assertTrue($this->sut->isExpired());
    }

    public function testLogoutSingle(): void
    {
        $ai = m::mock('Smorken\GoogleAuth\Model\VO\AuthItem');
        $this->mockInstance(Manager::class)->shouldReceive('getAuthItem')->andReturn($ai);
        $t = m::mock('Smorken\GoogleAuth\Token');
        $ai->shouldReceive('getToken')->andReturn($t);
        $t->shouldReceive('revoke');
        $ai->shouldReceive('isExpired')->andReturn(true);
        $t->shouldReceive('isExpired')->andReturn(true);
        $this->sut->logout(AuthAlias::PUBLIC_SERVER);
        $this->assertTrue($this->sut->isExpired(AuthAlias::PUBLIC_SERVER));
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->sut = new Auth($this->mockInstance(Manager::class));
    }
}
