<?php

namespace Tests\Smorken\GoogleAuth\Unit;

use Mockery as m;
use Smorken\GoogleAuth\Contracts\Auth as AuthAlias;
use Smorken\GoogleAuth\Model\VO\AccessToken;
use Smorken\GoogleAuth\Model\VO\AuthItem;
use Smorken\GoogleAuth\Token;
use Tests\Smorken\GoogleAuth\GoogleAuthTestCase;

class TokenTest extends GoogleAuthTestCase
{

    /**
     * @var Token|null
     */
    protected ?Token $sut = null;

    public function testRevoke(): void
    {
        $client = m::mock(\Google_Client::class);
        $client->shouldReceive('revokeToken');
        $ai = $this->mockAi($client);
        $ai->shouldReceive('getType')->andReturn(AuthAlias::PUBLIC_SERVER);
        $this->sut->setAuthItem($ai);
        $this->sut->revoke();
        $this->assertNull($this->sut->getToken());
    }

    public function testTokenRequestNoTokenRequired(): void
    {
        $client = m::mock(\Google_Client::class);
        $ai = $this->mockAi($client);
        $ai->shouldReceive('getType')->andReturn(AuthAlias::PUBLIC_SERVER);
        $this->sut->setAuthItem($ai);
        $this->assertTrue($this->sut->tokenRequest());
    }

    public function testTokenRequestTokenRequiredAndExists(): void
    {
        $client = m::mock(\Google_Client::class);
        $client->shouldReceive('getAccessToken')->andReturn(['token' => 'foo']);
        $client->shouldReceive('isAccessTokenExpired')->andReturn(false);
        $ai = $this->mockAi($client);
        $ai->shouldReceive('getType')->andReturn(AuthAlias::OAUTH_SERVICE);
        $this->sut->setAuthItem($ai);
        $this->assertInstanceOf(AccessToken::class, $this->sut->tokenRequest());
    }

    public function testTokenRequestTokenRequiredAndNotExists(): void
    {
        $client = m::mock(\Google_Client::class);
        $client->shouldReceive('getAccessToken')->andReturn([], ['token' => 'foo']);
        $client->shouldReceive('fetchAccessTokenWithAssertion');
        $ai = $this->mockAi($client);
        $ai->shouldReceive('getType')->andReturn(AuthAlias::OAUTH_SERVICE);
        $ai->shouldReceive('getAssertionCredentials')->andReturn(new \stdClass());
        $this->sut->setAuthItem($ai);
        $this->assertInstanceOf(AccessToken::class, $this->sut->tokenRequest());
    }

    /**
     * @param $client
     * @return m\LegacyMockInterface|m\MockInterface|AuthItem
     */
    protected function mockAi(\Google_Client $client): AuthItem|m\Mock
    {
        $m = m::mock(AuthItem::class);
        $m->shouldReceive('getConfigItem')->with('session_token', 'gauth')->andReturn('gauth');
        $m->shouldReceive('getClient')->andReturn($client);
        return $m;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->sut = new Token($this->app['session']);
    }
}
