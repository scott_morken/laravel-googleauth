<?php

namespace Tests\Smorken\GoogleAuth\Unit;

use Mockery as m;
use Smorken\GoogleAuth\Contracts\Auth as AuthAlias;
use Smorken\GoogleAuth\Factory;
use Smorken\GoogleAuth\Model\VO\AuthItem;
use Tests\Smorken\GoogleAuth\GoogleAuthTestCase;

class FactoryTest extends GoogleAuthTestCase
{

    /**
     * @var Factory|null
     */
    protected ?Factory $sut = null;

    public function testMakeOauthServiceNoUser(): void
    {
        $client = $this->mockClient();
        $this->clientMockMethods($client);
        $config = $this->getConfig();
        $config['access_type'] = AuthAlias::OAUTH_SERVICE;
        $expected_config = [
            'type' => 'service_account',
            'client_id' => 'randomstring.apps.googleusercontent.com',
            'client_email' => 'randomstring@developer.gserviceaccount.com',
        ];
        $client->shouldReceive('setAuthConfig')
               ->once()
               ->with(m::type('array'))
               ->andReturnUsing(function ($config) use ($expected_config) {
                   $ok = true;
                   foreach ($expected_config as $k => $v) {
                       if ($config[$k] !== $v) {
                           $ok = false;
                       }
                   }
                   return $ok;
               });
        $authitem = $this->sut->make($client, AuthAlias::OAUTH_SERVICE, $config);
        $this->assertInstanceOf('Smorken\GoogleAuth\Model\VO\AuthItem', $authitem);
        $this->assertEquals($client, $authitem->getClient());
    }

    public function testMakeOauthServiceWithUser(): void
    {
        $client = $this->mockClient();
        $this->clientMockMethods($client);
        $config = $this->getConfig();
        $config['access_type'] = AuthAlias::OAUTH_SERVICE;
        $expected_config = [
            'type' => 'service_account',
            'client_id' => 'randomstring.apps.googleusercontent.com',
            'client_email' => 'randomstring@developer.gserviceaccount.com',
        ];
        $client->shouldReceive('setAuthConfig')
               ->once()
               ->with(m::type('array'))
               ->andReturnUsing(function ($config) use ($expected_config) {
                   $ok = true;
                   foreach ($expected_config as $k => $v) {
                       if ($config[$k] !== $v) {
                           $ok = false;
                       }
                   }
                   return $ok;
               });
        $client->shouldReceive('setSubject')->once()->with('test');
        $authitem = $this->sut->make($client, AuthAlias::OAUTH_SERVICE, $config, 'test');
        $this->assertInstanceOf('Smorken\GoogleAuth\Model\VO\AuthItem', $authitem);
        $this->assertEquals($client, $authitem->getClient());
    }

    public function testMakeOauthWeb(): void
    {
        $client = $this->mockClient();
        $this->clientMockMethods($client);
        $config = $this->getConfig();

        $client->shouldReceive('setClientId')->with($config['client_id']);
        $client->shouldReceive('setClientSecret')->with($config['client_secret']);
        $client->shouldReceive('setScopes')->with($config['scopes']);
        $client->shouldReceive('setRedirectUri')->with($config['redirect_uri']);

        $config['access_type'] = AuthAlias::OAUTH_WEB;
        $authitem = $this->sut->make($client, AuthAlias::OAUTH_WEB, $config);
        $this->assertInstanceOf(AuthItem::class, $authitem);
        $this->assertEquals($client, $authitem->getClient());
    }

    public function testMakePublicBrowser(): void
    {
        $client = $this->mockClient();
        $this->clientMockMethods($client);
        $config = $this->getConfig();
        $client->shouldReceive('setDeveloperKey')->with($config['developer_key']);
        $config['access_type'] = AuthAlias::PUBLIC_BROWSER;
        $authitem = $this->sut->make($client, AuthAlias::PUBLIC_BROWSER, $config);
        $this->assertInstanceOf(AuthItem::class, $authitem);
        $this->assertEquals($client, $authitem->getClient());
    }

    public function testMakePublicServer(): void
    {
        $client = $this->mockClient();
        $this->clientMockMethods($client);
        $config = $this->getConfig();
        $client->shouldReceive('setDeveloperKey')->with($config['developer_key']);
        $authitem = $this->sut->make($client, AuthAlias::PUBLIC_SERVER, $this->getConfig());
        $this->assertInstanceOf(AuthItem::class, $authitem);
        $this->assertEquals($client, $authitem->getClient());
    }

    protected function clientMockMethods(\Google_Client|m\Mock $client): void
    {
        $client->shouldReceive('setApplicationName')->with(null);
        $client->shouldReceive('setAccessType')->with('offline');
        $client->shouldReceive('setScopes')->with(m::type('array'));
    }

    /**
     * @return \Google_Client|m\LegacyMockInterface|m\MockInterface
     */
    protected function mockClient(): \Google_Client
    {
        return $this->mockInstance(\Google_Client::class, false);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->sut = new Factory($this->app['files'], $this->app['session']);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
