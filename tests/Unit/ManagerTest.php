<?php

namespace Tests\Smorken\GoogleAuth\Unit;

use Mockery as m;
use Smorken\GoogleAuth\Auth;
use Smorken\GoogleAuth\Client;
use Smorken\GoogleAuth\Contracts\Auth as AuthAlias;
use Smorken\GoogleAuth\Manager;
use Smorken\GoogleAuth\Model\VO\AuthItem;
use Tests\Smorken\GoogleAuth\GoogleAuthTestCase;

class ManagerTest extends GoogleAuthTestCase
{

    /**
     * @var \Smorken\GoogleAuth\Contracts\Manager|null
     */
    protected ?\Smorken\GoogleAuth\Contracts\Manager $sut = null;

    public function testGetAuthItemDefault(): void
    {
        $client = $this->mockInstance(\Google_Client::class);
        $client->shouldReceive('setApplicationName')->with(null);
        $client->shouldReceive('setAccessType')->with($this->getConfig()['access_mode']);
        $client->shouldReceive('setDeveloperKey')->with($this->getConfig()['developer_key']);
        $client->shouldReceive('setAuthConfig')->with(m::type('array'));
        $ai = $this->sut->getAuthItem();
        $this->assertInstanceOf(AuthItem::class, $ai);
    }

    public function testGetAuthItemSpecifyType(): void
    {
        $client = $this->mockInstance(\Google_Client::class);
        $client->shouldReceive('setApplicationName')->with(null);
        $client->shouldReceive('setAccessType')->with($this->getConfig()['access_mode']);
        $expected_config = [
            'type' => 'service_account',
            'client_id' => 'randomstring.apps.googleusercontent.com',
            'client_email' => 'randomstring@developer.gserviceaccount.com',
        ];
        $client->shouldReceive('setAuthConfig')
               ->once()
               ->with(m::type('array'))
               ->andReturnUsing(function ($config) use ($expected_config) {
                   $ok = true;
                   foreach ($expected_config as $k => $v) {
                       if ($config[$k] !== $v) {
                           $ok = false;
                       }
                   }
                   return $ok;
               });
        $ai = $this->sut->getAuthItem(AuthAlias::OAUTH_SERVICE);
        $this->assertInstanceOf(AuthItem::class, $ai);
    }

    /**
     * @expectException \InvalidArgumentException
     */
    public function testGetAuthItemSpecifyTypeCanThrowException(): void
    {
        $client = $this->mockInstance(\Google_Client::class);
        $client->shouldReceive('setApplicationName')->with(null);
        $client->shouldReceive('setAccessType')->with($this->getConfig()['access_mode']);
        $client->shouldReceive('setDeveloperKey')->with($this->getConfig()['developer_key']);
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid access type [foo] given');
        $ai = $this->sut->getAuthItem('foo');
    }

    public function testGetClientIsClonedCopy(): void
    {
        $client = $this->mockInstance(\Google_Client::class);
        $client->shouldReceive('setApplicationName')->with(null);
        $client->shouldReceive('setAccessType')->with($this->getConfig()['access_mode']);
        $client->shouldReceive('setDeveloperKey')->with($this->getConfig()['developer_key']);
        $client->shouldReceive('setAuthConfig')->with(m::type('array'));
        $c = $this->sut->getClient();
        $this->assertNotSame($client, $c);
    }

    public function testGetClientSpecifyTypeIsClonedCopy(): void
    {
        $client = $this->mockInstance(\Google_Client::class);
        $client->shouldReceive('setApplicationName')->with(null);
        $client->shouldReceive('setAccessType')->with($this->getConfig()['access_mode']);
        $expected_config = [
            'type' => 'service_account',
            'client_id' => 'randomstring.apps.googleusercontent.com',
            'client_email' => 'randomstring@developer.gserviceaccount.com',
        ];
        $client->shouldReceive('setAuthConfig')
               ->once()
               ->with(m::type('array'))
               ->andReturnUsing(function ($config) use ($expected_config) {
                   $ok = true;
                   foreach ($expected_config as $k => $v) {
                       if ($config[$k] !== $v) {
                           $ok = false;
                       }
                   }
                   return $ok;
               });
        $c = $this->sut->getClient(AuthAlias::OAUTH_SERVICE);
        $this->assertNotSame($client, $c);
    }

    public function testGetConfigAsDefault(): void
    {
        $c = $this->sut->getConfig();
        $this->assertEquals($this->getConfig(), $c);
    }

    public function testGetConfigAsDefaultSpecifyType(): void
    {
        $c = $this->sut->getConfig('foo');
        $this->assertEquals($this->getConfig(), $c);
    }

    public function testGetScopesAsDefault(): void
    {
        $scopes = $this->sut->getScopes();
        $this->assertEquals($this->getConfig()['scopes'], $scopes);
    }

    public function testGetScopesAsDefaultSpecifyType(): void
    {
        $scopes = $this->sut->getScopes('foo');
        $this->assertEquals($this->getConfig()['scopes'], $scopes);
    }

    public function testImpersonateIsFalse(): void
    {
        $client = $this->mockInstance(\Google_Client::class);
        $client->shouldReceive('setApplicationName')->with(null);
        $client->shouldReceive('setAccessType')->with($this->getConfig()['access_mode']);
        $client->shouldReceive('setDeveloperKey')->with($this->getConfig()['developer_key']);
        $client->shouldReceive('setAuthConfig')->with(m::type('array'));
        $this->assertFalse($this->sut->impersonate('test', AuthAlias::PUBLIC_BROWSER));
    }

    public function testImpersonateIsTrue(): void
    {
        $client = $this->mockInstance(\Google_Client::class);
        $client->shouldReceive('setApplicationName')->with(null);
        $client->shouldReceive('setAccessType')->with($this->getConfig()['access_mode']);
        $expected_config = [
            'type' => 'service_account',
            'client_id' => 'randomstring.apps.googleusercontent.com',
            'client_email' => 'randomstring@developer.gserviceaccount.com',
        ];
        $client->shouldReceive('setAuthConfig')
               ->once()
               ->with(m::type('array'))
               ->andReturnUsing(function ($config) use ($expected_config) {
                   $ok = true;
                   foreach ($expected_config as $k => $v) {
                       if ($config[$k] !== $v) {
                           $ok = false;
                       }
                   }
                   return $ok;
               });
        $client->shouldReceive('setSubject')->once()->with('test');
        $this->assertTrue($this->sut->impersonate('test', AuthAlias::OAUTH_SERVICE));
    }

    public function testSetConfigMerges(): void
    {
        $config = [
            'access_type' => AuthAlias::OAUTH_SERVICE,
        ];
        $this->sut->setConfig($config);
        $this->assertEquals(AuthAlias::OAUTH_SERVICE, $this->sut->getConfig()['access_type']);
    }

    public function testSetConfigMergesSpecifyType(): void
    {
        $config = [
            'access_type' => AuthAlias::OAUTH_SERVICE,
        ];
        $this->sut->setConfig($config, 'foo');
        $this->assertEquals(AuthAlias::OAUTH_SERVICE, $this->sut->getConfig('foo')['access_type']);
    }

    public function testSetConfigMergesSpecifyTypeDifferentFromDefault(): void
    {
        $this->sut->setConfig($this->getConfigArray());
        $config = [
            'access_type' => AuthAlias::OAUTH_SERVICE,
        ];
        $this->sut->setConfig($config, 'foo');
        $this->assertEquals(AuthAlias::OAUTH_SERVICE, $this->sut->getConfig('foo')['access_type']);
        $this->assertEquals(AuthAlias::PUBLIC_SERVER, $this->sut->getConfig()['access_type']);
    }

    public function testSetScopes(): void
    {
        $scopes = [
            'foo',
            'bar',
        ];
        $this->sut->setScopes($scopes);
        $this->assertEquals($scopes, $this->sut->getScopes());
    }

    public function testSetScopesSpecifyType(): void
    {
        $scopes = [
            'foo',
            'bar',
        ];
        $this->sut->setScopes($scopes, 'fiz');
        $this->assertEquals($scopes, $this->sut->getScopes('fiz'));
    }

    public function testSetScopesSpecifyTypeDifferentFromDefault(): void
    {
        $scopes = [
            'foo',
            'bar',
        ];
        $this->sut->setScopes($scopes, 'fiz');
        $this->assertEquals($scopes, $this->sut->getScopes('fiz'));
        $this->assertEquals($this->getConfig()['scopes'], $this->sut->getScopes());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $c = new \Illuminate\Config\Repository(['googleauth' => $this->getConfig()]);
        $client = new Client($this->mockInstance(\Google_Client::class, false));
        $this->mockInstance(\Google_Client::class)->shouldReceive('setScopes')->with(m::type('array'));
        $factory = new \Smorken\GoogleAuth\Factory($this->app['files'], $this->app['session']);
        //$factory = m::mock('Smorken\GoogleAuth\GoogleAuthFactory', [$this->app])->makePartial();
        $this->sut = new Manager($client, $factory, $c);
    }
}
