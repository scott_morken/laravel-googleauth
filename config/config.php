<?php
return array(
    /*
    |--------------------------------------------------------------------------
    | Google access type
    |--------------------------------------------------------------------------
    |
    | access types are oauth_service, oauth_web, public_server, public_browser
    | service requires a service account and associated p12 file
    | web is normal oauth2 authentication
    |
    */
    'access_type' => env('GOOGLE_ACCESS_TYPE', 'public_server'),

    /*
    |--------------------------------------------------------------------------
    | Google session token name
    |--------------------------------------------------------------------------
    |
    | This is the session token name
    |
    */
    'session_token' => 'gauth_token',

    /*
    |--------------------------------------------------------------------------
    | Google client_id (oauth_web, oauth_service)
    |--------------------------------------------------------------------------
    |
    | Visit https://code.google.com/apis/console?api=plus to generate your
    | oauth2_client_id, oauth2_client_secret
    |
    */
    'client_id' => env('GOOGLE_CLIENT_ID', null),
    /*
    |--------------------------------------------------------------------------
    | Google client_secret (oauth_web)
    |--------------------------------------------------------------------------
    */
    'client_secret' => env('GOOGLE_CLIENT_SECRET', null),
    /*
    |--------------------------------------------------------------------------
    | Google Developer Key (public_browser, public_server)
    |--------------------------------------------------------------------------
    |
    | This is the public api key for either server or browser applications
    |
    */
    'developer_key' => env('GOOGLE_DEV_KEY', null),
    /*
    |--------------------------------------------------------------------------
    | Application Name (all)
    |--------------------------------------------------------------------------
    |
    | The Application Name to register with the Google API.
    |
    */
    'application_name' => env('GOOGLE_APP_NAME', null),
    /*
    |--------------------------------------------------------------------------
    | Redirect URI (oauth_web)
    |--------------------------------------------------------------------------
    */
    'redirect_uri' => env('GOOGLE_REDIRECT_URI', 'http://localhost'),
    /*
    |--------------------------------------------------------------------------
    | Scopes (all)
    |--------------------------------------------------------------------------
    |
    | An array of scopes to be requested during authentication.
    | For information about available login scopes, see
    | https://developers.google.com/+/api/oauth#login-scopes.
    | To see the available scopes for all Google APIs, visit the
    | APIs Explorer at https://developers.google.com/apis-explorer/#p/ .
    |
    */
    'scopes' => explode(',', env('GOOGLE_SCOPES', 'https://www.googleapis.com/auth/userinfo.profile')),

    /*
    |--------------------------------------------------------------------------
    | access_mode (all)
    |--------------------------------------------------------------------------
    |
    | The effect of this property is documented at
    | https://developers.google.com/accounts/docs/OAuth2WebServer#offline;
    | if an access token is being requested, the client does not receive
    | a refresh token unless offline is specified.
    | Possible values for access_type include:
    | "offline" to request offline access from the user. (This is the default value)
    | "online" to request online access from the user.
    |
    */
    'access_mode' => env('GOOGLE_ACCESS_MODE', 'offline'),

    /*
    |--------------------------------------------------------------------------
    | Google email_address (oauth_service)
    |--------------------------------------------------------------------------
    |
    | This is provided by the service account details
    |
    */
    'email_address' => env('GOOGLE_EMAIL_ADDRESS', null),

    /*
    |--------------------------------------------------------------------------
    | Google path_to_p12 (oauth_service)
    |--------------------------------------------------------------------------
    | This file is provided when you create a new service account. This file
    | needs to be installed on the web server and be readable by the web user.
    |
    */
    'path_to_p12' => env('GOOGLE_PATH_TO_P12', null),

    'p12_password' => env('GOOGLE_P12_PASSWORD', ''),

    /*
    |--------------------------------------------------------------------------
    | Google credentials_file (oauth_service)
    |--------------------------------------------------------------------------
    | This file is provided when you create a new service account. This file
    | needs to be installed on the web server and be readable by the web user.
    | This file can be used in place of the assertion credentials. (path_to_p12, etc).
    |
    */
    'credentials_file' => env('GOOGLE_APPLICATION_CREDENTIALS', null),
);
